#!/bin/bash 
#
#  Build SDK recovery image
#

untar_rootfs()
{
    echo "   1. Uncompress rootfs                           "
    echo "=================================================="
    cd $WRKD
    if [ -e "$WRKD/../$SDK_ROOTFS_DIR" ] ; then
        sudo rm -fr $WRKD/../$SDK_ROOTFS_DIR
        if [ $? -ne 0 ]
        then
            echo "====> Failed remove - $WRKD/../$SDK_ROOTFS_DIR"
            rm -fr $WRKD/../$SDK_SOURCES_DIR/$PROD_HIDDEN_FILE
            sdk_exit
        fi
    fi

    mkdir $WRKD/../$SDK_ROOTFS_DIR
    sdk_path_verification $WRKD/../$SDK_FS_DIR/$SDK_ROOTFS
    echo "====> sudo tar xf $WRKD/../$SDK_FS_DIR/$SDK_ROOTFS -C $WRKD/../$SDK_ROOTFS_DIR"
    sudo tar xf $WRKD/../$SDK_FS_DIR/$SDK_ROOTFS -C $WRKD/../$SDK_ROOTFS_DIR

    sudo mkdir -p $WRKD/../$SDK_ROOTFS_DIR/$XML_PARAMS_DIR
    sudo rm -fr $WRKD/../$SDK_ROOTFS_DIR/$XML_PARAMS_DIR/*
    sudo rm -f $WRKD/../$SDK_ROOTFS_DIR/boot/*
}

# Parameter - $1 - .config
kernel_comp()
{
    echo "   2. Kernel compilation                          "
    echo "=================================================="
    echo "====> cd $WRKD/$SDK_KERNEL_DIR/$SDK_KERNEL"
    cd $WRKD/$SDK_KERNEL_DIR/$SDK_KERNEL

    echo "====> sdk_kernel_comp $1"
    sdk_kernel_comp $1
}

# Parameter - $1 - MMP SDK name
#             $2 - w/o make clean
mmp_comp()
{
    if [ -e "$WRKD/$SDK_MMP_DIR" ] ; then
        echo "   3. MMP compilation                             "
        echo "=================================================="
        echo "====> cd $WRKD/$SDK_MMP_DIR"
        cd $WRKD/$SDK_MMP_DIR

        if [ "$2" == "" ] ; then
            if [ -e "$WRKD/$SDK_MMP_DIR/$SDK_MMP" ] ; then
                echo "====> rm -fr $WRKD/$SDK_MMP_DIR/$SDK_MMP"
                sudo rm -fr $WRKD/$SDK_MMP_DIR/$SDK_MMP
                if [ $? -ne 0 ]
                then
                    echo "====> Failed remove - $WRKD/$SDK_MMP_DIR/$SDK_MMP"
                    sdk_exit
                fi
            fi

            if [ "$1" = "" ] ; then
                echo "====> MMP SDK compressed file for $SDK_PROD not found"
                rm -fr $WRKD/../$SDK_SOURCES_DIR/$PROD_HIDDEN_FILE
                sdk_exit
            fi

            sdk_untar_file $1
            if [ $? -ne 0 ]
            then
                echo "Failed uncompress - $WRKD/$SDK_MMP_DIR/$1"
                rm -fr $WRKD/../$SDK_SOURCES_DIR/$PROD_HIDDEN_FILE
                sdk_exit
            fi
        fi

        cd $SDK_MMP
        echo "====> sdk_mmp_comp $WRKD/../$SDK_ROOTFS_DIR $WRKD/$SDK_KERNEL_DIR/$SDK_KERNEL"
        sdk_mmp_comp $WRKD/../$SDK_ROOTFS_DIR $WRKD/$SDK_KERNEL_DIR/$SDK_KERNEL $2

        echo "====> sdk_copy_mmp_files_to_fs $WRKD/$SDK_MMP_DIR/$SDK_MMP $WRKD/../$SDK_ROOTFS_DIR"
        sdk_copy_mmp_files_to_fs $WRKD/$SDK_MMP_DIR/$SDK_MMP $WRKD/../$SDK_ROOTFS_DIR
    fi
}

#Parameter - $1 - file system
sw_tree_comp()
{
    echo "   4. SW tree compilation                         "
    echo "=================================================="
    echo "====> cd $WRKD/$SDK_APPL_DIR/$SDK_APPL"
    cd $WRKD/$SDK_APPL_DIR/$SDK_APPL

    echo "====> sdk_swtree_comp $1"
    sdk_swtree_comp $1

    echo "====> sdk_copy_swtree_files_to_fs $WRKD/$SDK_APPL_DIR/$SDK_APPL $WRKD/../$SDK_ROOTFS_DIR"
    sdk_copy_swtree_files_to_fs $WRKD/$SDK_APPL_DIR/$SDK_APPL $WRKD/../$SDK_ROOTFS_DIR

    echo "====> sudo cp -fr $WRKD/$SDK_CONFIG_DIR/$XML_CONFIG_DIR/* $WRKD/../$SDK_ROOTFS_DIR/$XML_PARAMS_DIR/."
    sudo cp -fr $WRKD/$SDK_CONFIG_DIR/$XML_CONFIG_DIR/* $WRKD/../$SDK_ROOTFS_DIR/$XML_PARAMS_DIR/.
}

# Parameters - $1 - Avanta MC
#              $2 - Reduced FS
build_rcvr()
{
    echo "   6. Build recovery image                        "
    echo "=================================================="
    echo "====> cd $WRKD"
    cd $WRKD
    mkdir -p ../$SDK_RCVR_DIR

    sdk_path_verification $SDK_CONFIG_DIR/$RCS_CONFIG_DIR
    n=`ls $SDK_CONFIG_DIR/$CONFIG_DIR | wc -l`
    if [ $n -ne 0 ] ; then
        sudo cp -fr $SDK_CONFIG_DIR/$RCS_CONFIG_DIR/* ../$SDK_ROOTFS_DIR/$ETC_RCS_DIR/.
    else
        echo "====> No RCS scripts in $WRKD/$SDK_CONFIG_DIR/$RCS_CONFIG_DIR"
        sdk_exit
    fi

    sdk_path_verification $SDK_CONFIG_DIR/$START_CONFIG_FILE
    echo "sudo cp -f $SDK_CONFIG_DIR/$START_CONFIG_FILE ../$SDK_ROOTFS_DIR/usr/bin/."
    sudo cp -f $SDK_CONFIG_DIR/$START_CONFIG_FILE ../$SDK_ROOTFS_DIR/usr/bin/.

    if [ "$1" == "YES" ];then
        echo "====> Creating data for RW JFFS"
        if [ -e "../$JFFS_DIR" ];then
            rm -fr ../$JFFS_DIR
        fi
        mkdir -p ../$JFFS_DIR
        cp -fr ../$SDK_ROOTFS_DIR/$SDK_VAR_DIR/* ../$JFFS_DIR/.
        cp -fr $SDK_CONFIG_DIR/$XML_CONFIG_DIR ../$JFFS_DIR/.
        cp -fr $SDK_CONFIG_DIR/$RCS_CONFIG_DIR ../$JFFS_DIR/.
        if [ -e "$SDK_MMP_DIR/$SDK_MMP" ] ; then
            cp -fr $SDK_MMP_DIR/$SDK_MMP/$SDK_MMP_OUTPUT/*.xml ../$JFFS_DIR/$SDK_XML_CONFIG_DIR/.
        fi
        sdk_create_dropbear_link ../$SDK_ROOTFS_DIR YES
        echo "====> sdk_mc_rcvr_build $SDK_SCRIPTS_DIR ../$SDK_ROOTFS_DIR $SDK_KERNEL_DIR/$SDK_KERNEL $2"
        sdk_mc_rcvr_build $SDK_SCRIPTS_DIR ../$SDK_ROOTFS_DIR $SDK_KERNEL_DIR/$SDK_KERNEL $2
        mv $SDK_IMAGE ../$SDK_RCVR_DIR/.
        rm -fr $SDK_SQUASH_FS
        rm -fr $SDK_JFFS2_IMAGE
        rm -fr *.padded
    else
        echo "====> sdk_kw2_rcvr_build $SDK_SCRIPTS_DIR ../$SDK_ROOTFS_DIR $SDK_KERNEL_DIR/$SDK_KERNEL"
        sdk_kw2_rcvr_build $SDK_SCRIPTS_DIR ../$SDK_ROOTFS_DIR $SDK_KERNEL_DIR/$SDK_KERNEL
        mv $SDK_SQUASH_FS_IMAGE ../$SDK_RCVR_DIR/.
        rm -fr $SDK_SQUASH_FS
    fi
}

tar_rootfs()
{
    echo "   6. Compress rootfs                             "
    echo "=================================================="
    echo "====> cd $WRKD"
    cd $WRKD
    mkdir -p ../$SDK_FS_DIR

    echo "====> sudo cp -fr $SDK_KERNEL_DIR/$SDK_KERNEL/arch/arm/boot/uImage ../$SDK_ROOTFS_DIR/boot/."
    sudo cp -fr $SDK_KERNEL_DIR/$SDK_KERNEL/arch/arm/boot/uImage ../$SDK_ROOTFS_DIR/boot/.

    echo "====> cd ../$SDK_ROOTFS_DIR"
    cd ../$SDK_ROOTFS_DIR
    echo "====> sudo tar -cf $SDK_ROOTFS *"
    sudo tar -cf $SDK_ROOTFS *

    echo "====> sudo cp -fr $SDK_ROOTFS $WRKD/../$SDK_FS_DIR/."
    sudo cp -fr $SDK_ROOTFS $WRKD/../$SDK_FS_DIR/.
    sudo rm -fr $SDK_ROOTFS
}

print_help()
{
    echo ""
    echo " Build SDK recovery image"
    echo ""
    echo " ./sdkBuild.sh [-h | <product>]"
    echo ""
    echo "  <product>    - product type: "
    echo "                    $PROD_MC    - RD/DB SFU 88F6601 device special loopback mode"
    echo "                    $PROD_MC_NO_LPBK       - RD/DB SFU 88F6601 device"
    echo "                    $PROD_MC_REDUCED      - RD/DB SFU 88F6601 device with reduced file system"
    echo "                    $PROD_MC_VOIP  - RD/DB SFU 88F6601 device with VOIP"
    echo "                    $PROD_HGU          - RD GW 88F6560 device"
    echo "                    $PROD_SFU          - RD SFU 88F6510 device"
    echo "                    $PROD_SFU_REDUCED   - RD SFU 88F6510 device"
    echo "                    all"
    echo ""
    echo " Examples:"
    echo ""
    echo "   cd ...../SDK_..../Tools"
    echo "   ./sdkBuild.sh -h"
    echo "   ./sdkBuild.sh $PROD_MC"
    echo "   ./sdkBuild.sh all"
    echo "   ./sdkBuild.sh all >& /tmp/${USER}_sdk_build_`date +%d%m%y`.log"
    echo ""
}

# Set base definitions
if [ ! -e "sdkBase.sh" ];then
    echo "====> sdkBase.sh - unknown file/directory"
    exit 1
fi
source sdkBase.sh

if [ "$1" = "-h" ]
then
    print_help
    exit 1
fi

if [ $# -ne 1 ]
then
    echo "====> Invalid number of parameters "
    print_help
    exit 1
fi

# Include SDK utilities
if [ ! -e "sdkUtils.sh" ];then
    echo "====> sdkUtils.sh - unknown file/directory"
    exit 1
fi
. sdkUtils.sh

# Save scripts directory
SDK_SCRIPTS_DIR=`pwd`

# Save working directory
WRKD=$SDK_SCRIPTS_DIR/../$SDK_SOURCES_DIR
echo "====> Working directory is $WRKD"

CC_MC=NO
CC_KW2=NO
CC_MMP=NO
CC_RED=NO
SDK_MMP_SFU=
SDK_MMP_GW=
SDK_CURR_MMP=

if [ -e "$WRKD/$SDK_MMP_DIR" ] ; then
    cd $WRKD/$SDK_MMP_DIR

    for i in `ls *.tar.gz`
    do
        m=`echo $i | grep -c $SDK_MMP_FOR_GW`
        if [ $m -ne 0 ] ; then
            SDK_MMP_GW=$i
            echo "====> MMP SDK GW is $i"
        else
            SDK_MMP_SFU=$i
            echo "====> MMP SDK SFU is $i"
        fi
    done
fi

case $1 in
    "$PROD_MC")
        export SDK_PROD=$PROD_MC
        CC_MC=YES
        rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_IMAGE
    ;;
    "$PROD_MC_NO_LPBK")
        export SDK_PROD=$PROD_MC_NO_LPBK
        CC_MC=YES
        rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_IMAGE
    ;;
    "$PROD_MC_REDUCED")
        export SDK_PROD=$PROD_MC_REDUCED
        CC_MC=YES
        CC_RED=YES
        rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_IMAGE
    ;;
    "$PROD_MC_VOIP")
        export SDK_PROD=$PROD_MC_VOIP
        SDK_CURR_MMP=$SDK_MMP_SFU
        CC_MC=YES
        CC_MMP=YES
        rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_IMAGE
    ;;
    "$PROD_HGU")
        export SDK_PROD=$PROD_HGU
        SDK_CURR_MMP=$SDK_MMP_GW
        CC_KW2=YES
        CC_MMP=YES
        rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_SQUASH_FS_IMAGE
    ;;
    "$PROD_SFU")
        export SDK_PROD=$PROD_SFU
        SDK_CURR_MMP=$SDK_MMP_SFU
        CC_KW2=YES
        CC_MMP=YES
        rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_SQUASH_FS_IMAGE
    ;;
    "$PROD_SFU_REDUCED")
        export SDK_PROD=$PROD_SFU_REDUCED
        SDK_CURR_MMP=$SDK_MMP_SFU
        CC_KW2=YES
        CC_MMP=YES
        rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_SQUASH_FS_IMAGE
    ;;
    *)
        export SDK_PROD="all"
    ;;
esac

new_product=no
if [ -e "$WRKD/$PROD_HIDDEN_FILE" ];then
    hidden_prod=`cat $WRKD/$PROD_HIDDEN_FILE`

    if [ "$hidden_prod" != "$SDK_PROD" ];then
        echo "$SDK_PROD" > $WRKD/$PROD_HIDDEN_FILE
        new_product=yes
    fi
else
    echo "$SDK_PROD" > $WRKD/$PROD_HIDDEN_FILE
    new_product=yes
fi

if [ "$CROSS_COMPILE" = "" ] ; then
    echo "====> Invalid CROSS_COMPILE value"
    exit 1
fi

sdk_start_log

echo "=================================================="
echo "====> SDK compilation for $SDK_PROD product(s)    "
echo "====> SDK compilation for $SDK_PROD product(s)    " >> $SDK_LOGUTIL
echo "=================================================="

sdk_path_verification $WRKD/../$SDK_ROOTFS_DIR
sdk_path_verification $WRKD/../$SDK_TOOLS_DIR/mksquashfs
sdk_path_verification $WRKD/../$SDK_TOOLS_DIR/mkimage

if [ "$SDK_PROD" = "all" ] ; then
    echo "====> cd $WRKD/../$SDK_OUTPUT_DIR/$PRODUCTS_DIR"
    cd $WRKD/../$SDK_OUTPUT_DIR/$PRODUCTS_DIR

    for i in `ls`;do
        echo "====> Next product is $i"

        export SDK_PROD=$i
        source $SDK_SCRIPTS_DIR/sdkBase.sh
        sdk_path_verification $WRKD/../$SDK_FS_DIR

        CC_MC=NO
        CC_KW2=NO
        CC_MMP=NO
        CC_RED=NO
        SDK_CURR_MMP=

        case $i in
            "$PROD_MC")
                CC_MC=YES
                rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_IMAGE
            ;;
            "$PROD_MC_NO_LPBK")
                CC_MC=YES
                rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_IMAGE
            ;;
            "$PROD_MC_REDUCED")
                CC_MC=YES
                CC_RED=YES
                rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_IMAGE
            ;;
            "$PROD_MC_VOIP")
                SDK_CURR_MMP=$SDK_MMP_SFU
                CC_MC=YES
                CC_MMP=YES
                rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_IMAGE
            ;;
            "$PROD_HGU")
                SDK_CURR_MMP=$SDK_MMP_GW
                CC_KW2=YES
                CC_MMP=YES
                rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_SQUASH_FS_IMAGE
            ;;
            "$PROD_SFU")
                SDK_CURR_MMP=$SDK_MMP_SFU
                CC_KW2=YES
                CC_MMP=YES
                rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_SQUASH_FS_IMAGE
            ;;
            "$PROD_SFU_REDUCED")
                SDK_CURR_MMP=$SDK_MMP_SFU
                CC_KW2=YES
                CC_MMP=YES
                rm -fr $WRKD/../$SDK_RCVR_DIR/$SDK_SQUASH_FS_IMAGE
            ;;
        esac

        if [ -e "$WRKD/$SDK_CONFIG_DIR/$K_CONFIG_DIR/$DOTCONFIG_NAME" ];then
            echo "=================================================="
            echo "====> Build recovery for $SDK_PROD product        "
            echo "====> Build recovery for $SDK_PROD product        " >> $SDK_LOGUTIL
            echo "=================================================="
            untar_rootfs

            kernel_comp $WRKD/$SDK_CONFIG_DIR/$K_CONFIG_DIR/$DOTCONFIG_NAME
            if [ "$CC_MMP" == "YES" ];then
                mmp_comp $SDK_CURR_MMP
            fi
            sw_tree_comp $WRKD/../$SDK_ROOTFS_DIR
            rm -fr $WRKD/../$SDK_FS_DIR/$SDK_ROOTFS
            build_rcvr $CC_MC $CC_RED
            tar_rootfs
        else
            echo "====> Nothing is done for $SDK_PROD product" >> $SDK_LOGUTIL
        fi
    done
else
    source $SDK_SCRIPTS_DIR/sdkBase.sh
    sdk_path_verification $WRKD/../$SDK_FS_DIR

    WO_CLEAN=YES

    if [ -e "$WRKD/$SDK_CONFIG_DIR/$K_CONFIG_DIR/$DOTCONFIG_NAME" ];then
        if [ "$new_product" = "yes" ] ; then
            untar_rootfs
            kernel_comp $WRKD/$SDK_CONFIG_DIR/$K_CONFIG_DIR/$DOTCONFIG_NAME
            if [ "$CC_MMP" == "YES" ];then
                mmp_comp $SDK_CURR_MMP
            fi
            sw_tree_comp $WRKD/../$SDK_ROOTFS_DIR
        else
            kernel_comp
            if [ "$CC_MMP" == "YES" ];then
                mmp_comp $SDK_CURR_MMP $WO_CLEAN
            fi
            sw_tree_comp
        fi

        rm -fr $WRKD/../$SDK_FS_DIR/$SDK_ROOTFS
        build_rcvr $CC_MC $CC_RED
        tar_rootfs
    else
        echo "====> $WRKD/$SDK_CONFIG_DIR/$K_CONFIG_DIR/$DOTCONFIG_NAME - unknown file/directory"
    fi
fi

sdk_print_log

