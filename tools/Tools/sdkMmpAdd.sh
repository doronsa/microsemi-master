#!/bin/bash
#
#  Add SDK MMP package
#

print_help()
{
    echo ""
    echo " Add MMP package to SDK"
    echo ""
    echo " ./sdkMmpAdd.sh [-h | <MMP_sdk_path>]"
    echo ""
    echo "  <MMP_sdk_path> - full MMP SDK path"
    echo ""
    echo " Examples:"
    echo ""
    echo "   ./sdkMmpAdd.sh -h"
    echo "   ./sdkMmpAdd.sh /...../mmp_sdk_1.05.55_2574_linux.tar.gz"
    echo ""
}

if [ "$1" = "-h" ]
then
    print_help
    exit 1
fi

if [ $# -ne 1 ]
then
    echo "====> Invalid number of parameters "
    print_help
    exit 1
fi

# Set base definitions
if [ ! -e "sdkBase.sh" ];then
    echo "====> sdkBase.sh - unknown file/directory"
    exit 1
fi
source sdkBase.sh

# Save scripts directory
SDK_SCRIPTS_DIR=`pwd`

# Save working directory
WRKD=$SDK_SCRIPTS_DIR/../$SDK_SOURCES_DIR/$SDK_MMP_DIR
echo "====> MMP directory is $WRKD"

mkdir -p $WRKD
cd $WRKD
rm -fr *
cp $1 .

tar -xf $1
if [ $? -ne 0 ]
then
    echo "Failed uncompress - $1"
fi

