#!/bin/bash
#
# SDK utilities
#

sdk_add_new_files()
{
#doron add files
    sudo cp I2C_DIR/busybox_v2 $2/bin/.
    sudo cp I2C_DIR/i2c* $2/bin/.
    sudo chmod 777 $2/usr/i2c*
    cd $2/dev
    sudo mknod $2/dev/watchdog c 10 130

}

sdk_start_log()
{
    echo "========================================="> $SDK_LOGUTIL
    echo "`date` - start"                           >> $SDK_LOGUTIL
    echo "=========================================">> $SDK_LOGUTIL
    return
}

sdk_print_log()
{
    cat $SDK_LOGUTIL
    rm -f $SDK_LOGUTIL
    echo "========================================="
    echo "`date` - finish"
    echo "========================================="

    return
}

sdk_exit()
{
    rm -f $SDK_LOGUTIL
    exit 1
}

# Parameters:
#             $1 - $0 parameter of script
#
sdk_get_full_path()
{
    mdir=$(dirname $1)
    first_sbl=${mdir:0:1}
    if [ "$first_sbl" != "/" ]
    then
        mdir=`pwd`/$mdir
    fi

    echo $mdir
    return
}

# Parameters:
#             $1 - verified file or directory
#
# In case a path has not been defined, NULL data is moved as a parameter.
# -e test operator returns TRUE for NULL.
#
sdk_path_verification()
{
    if [ ! -e "$1" ]
    then
        echo "====> $1 - unknown file/directory"
        sdk_exit
    fi
    if [ "$1" = "" ]
    then
        echo "====> NULL path"
        sdk_exit
    fi
}


# Parameters:
#             $1 - full path of a tar file
#
sdk_untar_file()
{
    fname=`basename $1`
    dname=`dirname $1`
    fext=`echo $fname | tr '.' '\n' | tail -1`

    if [ -e $1 ]
    then
        echo "====> Uncompressing $1"
        if [ "$fext" = "bz2" ]
        then
            tar xjf $1 -C $dname
        elif [ "$fext" = "gz" ]
        then
            tar xzf $1 -C $dname
        elif [ "$fext" = "tgz" ]
        then
            tar xzf $1 -C $dname
        elif [ "$fext" = "lnk" ]
        then
            echo "====> Link instead of file: $1"
            sdk_exit
        else
            echo "====> Unknown type $fext of the compressed file $1"
            sdk_exit
        fi
    fi

    if [ $? -ne 0 ]
    then
        echo "Failed uncompress of $1"
        sdk_exit
    fi
}

# Parameters:
#             $1 - file name
#             $2 - string
#
sdk_cmp_name()
{
    if [ "$2" = "" ] ;then
        return 0
    else
       n=`echo $1 | grep -c $2`
       return $n
    fi
}

sdk_comp()
{
    echo "====> make CROSS_COMPILE=${CROSS_COMPILE}"
    make CROSS_COMPILE=${CROSS_COMPILE}
    if [ $? -ne 0 ]
    then
        echo "Compilation failed"
        sdk_exit
    fi
}

# Parameters:
#             $1 - kernel configuration file
#
sdk_kernel_comp()
{
    if [ "$1" != "" ] ;then
        echo "====> make mrproper ARCH=arm"
        make mrproper ARCH=arm

        echo "====> cp -fr $1 ."
        cp -fr $1 .
        if [ $? -ne 0 ]
        then
            echo "Failed copy - $1 to `pwd`"
            sdk_exit
        fi
    fi

    proc_num=`cat /proc/cpuinfo | grep "processor" | tail -1 | cut -f2 -d":"`
    if [ $? -ne 0 ]
    then
        echo "Failed to get number of processors"
        sdk_exit
    else
        echo "====> Kernel is compiled with $proc_num processes"
    fi

    echo "====> make uImage -j $proc_num  ARCH=arm CROSS_COMPILE=${CROSS_COMPILE}"
    make uImage -j $proc_num ARCH=arm CROSS_COMPILE=${CROSS_COMPILE}
    if [ $? -ne 0 ]
    then
        echo "Failed kernel compilation"
        sdk_exit
    fi
    ##echo "====> make modules -j $proc_num ARCH=arm CROSS_COMPILE=${CROSS_COMPILE}"
    ##make modules -j $proc_num ARCH=arm CROSS_COMPILE=${CROSS_COMPILE}
    ##if [ $? -ne 0 ]
    ##then
    ##    echo "Failed kernel modules compilation"
    ##    sdk_exit
    ##fi
}

# Parameters:
#             $1 - rootfs
#
sdk_swtree_comp()
{
    if [ "$1" != "" ] ;then
        echo "====> make clean CROSS_COMPILE=${CROSS_COMPILE} > /dev/null"
        make clean CROSS_COMPILE=${CROSS_COMPILE} > /dev/null
        if [ $? -ne 0 ]
        then
            echo "Failed SWTREE clean"
            sdk_exit
        fi
    fi

    sdk_comp
}

# Parameters:
#             $1 - rootfs dir
#             $2 - kernel dir
#             $3 - w/o clean
sdk_mmp_comp()
{
    if [ "$3" == "" ] ;then
        echo "====> make clean CROSS_COMPILE=${CROSS_COMPILE} ROOTFS=$1 KDIR=$2 > /dev/null"
        make clean CROSS_COMPILE=${CROSS_COMPILE} ROOTFS=$1 KDIR=$2 > /dev/null
        if [ $? -ne 0 ]
        then
            echo "Failed MMP clean"
            sdk_exit
        fi

        rm -fr $SDK_MMP_OUTPUT
    fi

    echo "====> make all CROSS_COMPILE=${CROSS_COMPILE} ROOTFS=$1 KDIR=$2"
    make all CROSS_COMPILE=${CROSS_COMPILE} ROOTFS=$1 KDIR=$2
    if [ $? -ne 0 ]
    then
        echo "Failed MMP compilation"
        sdk_exit
    fi
}

# Parameters:
#             $1 - recovery tools dir
#             $2 - rootfs dir
#             $3 - kernel dir
#
sdk_kw2_rcvr_build()
{
    sdk_path_verification $1
    sdk_path_verification $2
    sdk_path_verification $3

    ## KW2 recovery image MUST contain uImage in the rootfs
    echo "sudo cp -fr $3/arch/arm/boot/uImage $2/boot/."
    sudo cp -fr $3/arch/arm/boot/uImage $2/boot/.

    echo "$1/mksquashfs $2 $SDK_SQUASH_FS -noappend"
    $1/mksquashfs $2 $SDK_SQUASH_FS -noappend
    if [ $? -ne 0 ]
    then
        echo "KW2: Failed mksquashfs"
        sdk_exit
    fi

    echo "$1/mkimage -A arm -O linux -T multi -C none -n 'Linux Multiboot-Image' -e 0x00008000 -a 0x00008000 -d $3/arch/arm/boot/zImage:$SDK_SQUASH_FS $SDK_SQUASH_FS_IMAGE"
    $1/mkimage -A arm -O linux -T multi -C none -n 'Linux Multiboot-Image' -e 0x00008000 -a 0x00008000 -d $3/arch/arm/boot/zImage:$SDK_SQUASH_FS $SDK_SQUASH_FS_IMAGE
    if [ $? -ne 0 ]
    then
        echo "KW2: Failed mkimage"
        sdk_exit
    fi
}

# Parameters:
#             $1 - recovery tools dir
#             $2 - rootfs dir
#             $3 - kernel dir
#             $4 - reduced rootfs
#
sdk_mc_rcvr_build()
{
    sdk_path_verification $1
    sdk_path_verification $3
    sdk_path_verification $2/../$JFFS_DIR

    if [ "$4" == "YES" ]
    then
        JFFS_PAD=0x80000
        SQFS_IBS=3M
        UI_IBS=3584K
    else
        JFFS_PAD=0x200000
        SQFS_IBS=10M
        UI_IBS=3M
    fi

    echo "$1/mkfs.jffs2 -r $2/../$JFFS_DIR -e 64 --pad=$JFFS_PAD -o $SDK_JFFS2_IMAGE"
    $1/mkfs.jffs2 -r $2/../$JFFS_DIR -e 64 --pad=$JFFS_PAD -o $SDK_JFFS2_IMAGE
    if [ $? -ne 0 ]
    then
        echo "MC: Failed mkfs.jffs2"
        sdk_exit
    fi

    echo "$1/mksquashfs $2 $SDK_SQUASH_FS -noappend"
    $1/mksquashfs $2 $SDK_SQUASH_FS -noappend
    if [ $? -ne 0 ]
    then
        echo "MC: Failed mksquashfs"
        sdk_exit
    fi

    echo "dd if=$SDK_SQUASH_FS of=$SDK_SQUASH_FS_PAD ibs=$SQFS_IBS conv=sync"
    dd if=$SDK_SQUASH_FS of=$SDK_SQUASH_FS_PAD ibs=$SQFS_IBS conv=sync
    if [ $? -ne 0 ]
    then
        echo "MC: Failed dd for squashfs image"
        sdk_exit
    fi

    echo "dd if=$3/arch/arm/boot/uImage of=$SDK_UIMAGE_PAD ibs=$UI_IBS conv=sync"
    dd if=$3/arch/arm/boot/uImage of=$SDK_UIMAGE_PAD ibs=$UI_IBS conv=sync
    if [ $? -ne 0 ]
    then
        echo "MC: Failed dd for uimage"
        sdk_exit
    fi

    echo "cat $SDK_UIMAGE_PAD $SDK_SQUASH_FS_PAD $SDK_JFFS2_IMAGE > $SDK_IMAGE"
    cat $SDK_UIMAGE_PAD $SDK_SQUASH_FS_PAD $SDK_JFFS2_IMAGE > $SDK_IMAGE
    if [ $? -ne 0 ]
    then
        echo "MC: Failed cat SDK image"
        sdk_exit
    fi
}

# Parameters:
#             $1 - swtree dir
#             $2 - mmp dir
#
sdk_add_mmp_to_swtree()
{
    mkdir -p $1/mmp
    echo "====> cd $1/mmp"
    cd $1/mmp
    if [ $? -ne 0 ]
    then
        sdk_exit
    fi

    mkdir -p config
    sdk_path_verification $2/app/cli/xml/mmp_types.xml
    echo "====> cp -fr $2/app/cli/xml/mmp_types.xml config/."
    cp -fr $2/app/cli/xml/mmp_types.xml config/.
    sdk_path_verification $2/app/cli/xml/mmp_view.xml
    echo "====> cp -fr $2/app/cli/xml/mmp_view.xml config/."
    cp -fr $2/app/cli/xml/mmp_view.xml config/.

    mkdir -p incl
    sdk_path_verification $2/incl/mmp_api.h
    echo "====> cp -fr $2/incl/mmp_api.h incl/."
    cp -fr $2/incl/mmp_api.h incl/.
    sdk_path_verification $2/app/cli/cli/mmp_clish/libmmpcli.h
    echo "====> cp -fr $2/app/cli/cli/mmp_clish/libmmpcli.h incl/."
    cp -fr $2/app/cli/cli/mmp_clish/libmmpcli.h incl/.
    sdk_path_verification $2/app/cli/cli/mmp_clish/mmp_cli_cmd_list_file.h
    echo "====> cp -fr $2/app/cli/cli/mmp_clish/mmp_cli_cmd_list_file.h incl/."
    cp -fr $2/app/cli/cli/mmp_clish/mmp_cli_cmd_list_file.h incl/.

    mkdir -p lib
    sdk_path_verification $2/lib/libmmp.a
    echo "====> cp -fr $2/lib/libmmp.a lib/."
    cp -fr $2/lib/libmmp.a lib/.
    sdk_path_verification $2/app/cli/cli/mmp_clish/libmmpcli.a
    echo "====> cp -fr $2/app/cli/cli/mmp_clish/libmmpcli.a lib/."
    cp -fr $2/app/cli/cli/mmp_clish/libmmpcli.a lib/.
    sdk_path_verification $2/app/slic/libslic.a
    echo "====> cp -fr $2/app/slic/libslic.a lib/."
    cp -fr $2/app/slic/libslic.a lib/.
}

# Parameters:
#             $1 - kernel dir
#             $2 - rootfs dir
#
sdk_copy_kernel_files_to_fs()
{
    sdk_path_verification $1/arch/arm/boot/uImage
    sudo cp -fr $1/arch/arm/boot/uImage $2/boot/.
###    sdk_path_verification $1/arch/arm/boot/zImage
###    sudo cp -fr $1/arch/arm/boot/zImage $2/boot/.
}

# Parameters:
#             $1 - mmp dir
#             $2 - rootfs dir
#
# i2c path
I2C_DIR=/media/doronsa/457cd718-9daa-4ce7-ba1a-42d4d4842eb5/data/doron/workspace/marvel_Linux_v10.93.3.3/SDK_2.6.25-RC78/Source/Application/i2c-tool/i2c-tools-3.1.0/tools/
sdk_copy_mmp_files_to_fs()
{
    sudo mkdir -p $2/usr/bin/xml
    sdk_path_verification $1/$SDK_MMP_OUTPUT/xml
    sudo cp -fr $1/$SDK_MMP_OUTPUT/xml/*       $2/usr/bin/xml/.
    sudo cp -fr $1/$SDK_MMP_OUTPUT/mmp_*       $2/usr/bin/.
    sudo cp -fr $1/$SDK_MMP_OUTPUT/sipapp*     $2/usr/bin/.
    sdk_path_verification $1/$SDK_MMP_OUTPUT/mmp.ko
    sudo cp -fr $1/$SDK_MMP_OUTPUT/mmp.ko      $2/usr/sbin/.

###    sudo mkdir -p $2/$XML_PARAMS_DIR
###    sudo rm -f $2/$XML_PARAMS_DIR/*
    sdk_path_verification $1/$SDK_MMP_OUTPUT/mmp_cfg.xml
    sudo cp -fr $1/$SDK_MMP_OUTPUT/mmp_cfg.xml $2/$XML_PARAMS_DIR/.
    sdk_path_verification $1/$SDK_MMP_OUTPUT/sipapp.xml
    sudo cp -fr $1/$SDK_MMP_OUTPUT/sipapp.xml  $2/$XML_PARAMS_DIR/.
#doron add files
    sudo cp I2C_DIR/busybox_v2 $2/bin/.
    sudo cp I2C_DIR/i2c* $2/bin/.
    sudo chmod 777 $2/usr/i2c*
    cd $2/dev
    sudo mknod $2/dev/watchdog c 10 130
    
    
}

# Parameters:
#             $1 - swtree dir
#             $2 - rootfs dir
#
sdk_copy_swtree_files_to_fs()
{
    sudo mkdir -p $2/$XML_COMMANDS_DIR
    sudo rm -f $2/$XML_COMMANDS_DIR/*

    sudo cp -fr $1/$SDK_APPL_BUILD_BIN_DIR/*       $2/usr/bin/.
    sudo cp -fr $1/$SDK_APPL_BUILD_LIB_DIR/*       $2/usr/lib/.
    sudo cp -fr $1/$SDK_APPL_COMMANDS_DIR/*        $2/$XML_COMMANDS_DIR/.

    if [ -e "$1/$OMCI_TOOL_DIR/$OMCI_TOOL" ] ; then
        sudo cp -f  $1/$OMCI_TOOL_DIR/$OMCI_TOOL $2/usr/bin/.
    fi
}

# Parameters:
#             $1 - version number
#             $2 - product name
#             $3 - version file
#
sdk_create_version_file()
{
echo "====> chmod +w $3"
chmod +w $3

echo "/* Automatically generated file */

#ifndef _US_VERSION_H_
#define _US_VERSION_H_

#define SDK_VERSION \"$1-$2\"
#define SDK_VERSION_DATE \"`date +"%b %d %Y %T"`\"

#endif
" > $3
}

# Parameters:
#             $1 - start dir
#
sdk_remove_keep_files()
{
    for i in `find $1 -name "*.keep*"`
    do
        rm -fr $i
    done
}

# Parameters:
#           $1 - XML file name
#           $2 - XML parameter name
#           $3 - value
#
update_xml_parameter()
{
    cat $1 | sed -e 's/<'$2'>./<'$2'>'$3'/' > $XML_TMP_FILE
}

# Parameters:
#           $1 - rootfs dir
#           $2 - Avanta MC device
sdk_create_dropbear_link()
{
    if [ -e "$1/etc/dropbear" ];then
        echo "====> sudo rm -fr $1/etc/dropbear/*"
        sudo rm -fr $1/etc/dropbear/*
    else
        echo "====> sudo mkdir $1/etc/dropbear"
        sudo mkdir $1/etc/dropbear
    fi

    if [ "$2" == "YES" ];then
        echo "====> mkdir $1/$JFFS_DIR/dropbear"
        mkdir $1/../$JFFS_DIR/dropbear
    fi
}

#####
