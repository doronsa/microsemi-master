#!/bin/bash
#
#  SDK compilation
#

print_help()
{
    echo ""
    echo " SDK compilation"
    echo ""
    echo " ./sdkEnv.sh [-h] | <product>]"
    echo ""
    echo "  <product>    - product type: "
    echo "                    $PROD_MC    - RD/DB SFU 88F6601 device special loopback mode"
    echo "                    $PROD_MC_NO_LPBK       - RD/DB SFU 88F6601 device"
    echo "                    $PROD_MC_REDUCED      - RD/DB SFU 88F6601 device with reduced file system"
    echo "                    $PROD_MC_VOIP  - RD/DB SFU 88F6601 device with VOIP"
    echo "                    $PROD_HGU          - RD GW 88F6560 device"
    echo "                    $PROD_SFU          - RD SFU 88F6510 device"
    echo "                    $PROD_SFU_REDUCED   - RD SFU 88F6510 device"
    echo ""
    echo " Examples:"
    echo ""
    echo "   cd ...../SDK_..../Tools"
    echo "   ./sdkEnv.sh -h"
    echo "   ./sdkEnv.sh $PROD_MC"
    echo "   ./sdkEnv.sh >& /tmp/${USER}_sdkEnv_`date +%d%m%y_%T`.log"
    echo ""
}

# Set base definitions
if [ ! -e "sdkBase.sh" ];then
    echo "====> sdkBase.sh - unknown file/directory"
    exit 1
fi
source sdkBase.sh

if [ "$1" = "-h" ]
then
    print_help
    exit 1
fi

# Include SDK utilities
if [ ! -e "sdkUtils.sh" ];then
    echo "====> sdkUtils.sh - unknown file/directory"
    exit 1
fi
. sdkUtils.sh

# Save scripts directory
SDK_SCRIPTS_DIR=`pwd`

# Save working directory
WRKD=$SDK_SCRIPTS_DIR/../$SDK_SOURCES_DIR

sdk_start_log

echo "=================================================="
echo "====> Uncompressing sources                       "
echo "=================================================="
cd $WRKD
for i in `find . -name "*.tar*"`
do
    m=`echo $i | grep -c $SDK_KERNEL`
    if [ $m -ne 0 ] ; then
        if [ ! -e "$WRKD/$SDK_KERNEL_DIR/$SDK_KERNEL" ] ; then
            sdk_untar_file $i
        fi
    else
        m=`echo $i | grep -c $SDK_APPL`
        if [ $m -ne 0 ] ; then
            if [ ! -e "$WRKD/$SDK_APPL_DIR/$SDK_APPL" ];then
                sdk_untar_file $i
            fi
        fi
    fi
done

echo "=================================================="
echo "====> Working directory is $WRKD"
echo "=================================================="

SDK_PROD=$1

if [ "$SDK_PROD" == "" ] ; then
    sdk_path_verification $WRKD/$SDK_CONFIG_DIR/$PRODUCTS_DIR
    echo "====> cd $WRKD/$SDK_CONFIG_DIR/$PRODUCTS_DIR"
    cd $WRKD/$SDK_CONFIG_DIR/$PRODUCTS_DIR

    for i in `ls`;do
        if [ "$SDK_PROD" == "" ] ; then
            if [ -e "$WRKD/$SDK_CONFIG_DIR/$PRODUCTS_DIR/$i/Kernel/$DOTCONFIG_NAME" ] ; then
                SDK_PROD=$i
            fi
        fi
    done

    if [ "$SDK_PROD" == "" ] ; then
        echo "====> No .config file in $SDK_CONFIG_DIR"
        sdk_exit
    fi
fi

source $SDK_SCRIPTS_DIR/sdkBase.sh
cp $WRKD/$SDK_CONFIG_DIR/$K_CONFIG_DIR/$DOTCONFIG_NAME $WRKD/$SDK_KERNEL_DIR/$SDK_KERNEL/.

new_product=no
if [ -e "$WRKD/$PROD_HIDDEN_FILE" ];then
    hidden_prod=`cat $WRKD/$PROD_HIDDEN_FILE`

    if [ "$hidden_prod" != "$SDK_PROD" ];then
        echo "$SDK_PROD" > $WRKD/$PROD_HIDDEN_FILE
        new_product=yes
    fi
else
    echo "$SDK_PROD" > $WRKD/$PROD_HIDDEN_FILE
    new_product=yes
fi

if [ "$new_product" = "yes" ] ; then
    if [ -e "$WRKD/../$SDK_ROOTFS_DIR" ] ; then
        echo "====> sudo rm -fr $WRKD/../$SDK_ROOTFS_DIR"
        sudo rm -fr $WRKD/../$SDK_ROOTFS_DIR
        if [ $? -ne 0 ]
        then
            echo "====> Failed remove - $WRKD/../$SDK_ROOTFS_DIR"
            sdk_exit
        fi
    fi

    echo "====> Uncompressing $WRKD/../$SDK_FS_DIR/$SDK_ROOTFS"
    mkdir $WRKD/../$SDK_ROOTFS_DIR
    sdk_path_verification $WRKD/../$SDK_FS_DIR/$SDK_ROOTFS
    sudo tar -xf $WRKD/../$SDK_FS_DIR/$SDK_ROOTFS -C $WRKD/../$SDK_ROOTFS_DIR
    if [ $? -ne 0 ]
    then
        echo "====> Failed uncompress - $WRKD/../$SDK_FS_DIR/$SDK_ROOTFS"
        sdk_exit
    fi
    sudo mkdir -p $WRKD/../$SDK_ROOTFS_DIR/$XML_PARAMS_DIR
    sudo rm -fr $WRKD/../$SDK_ROOTFS_DIR/$XML_PARAMS_DIR/*
    sudo mkdir -p $WRKD/../$SDK_ROOTFS_DIR/boot
    sudo rm -f $WRKD/../$SDK_ROOTFS_DIR/boot/*

    if [ -e "$WRKD/$SDK_MMP_DIR" ] ; then
        if [ -e "$WRKD/$SDK_MMP_DIR/$SDK_MMP" ] ; then
            echo "====> rm -fr $WRKD/$SDK_MMP_DIR/$SDK_MMP"
            sudo rm -fr $WRKD/$SDK_MMP_DIR/$SDK_MMP
            if [ $? -ne 0 ]
            then
                echo "====> Failed remove - $WRKD/$SDK_MMP_DIR/$SDK_MMP"
                sdk_exit
            fi
        fi
    fi
fi

cross_used=`echo $CROSS_COMPILE | grep -c $SDK_CROSS_UCLIBC_NAME`
if [ "$cross_used" != "1" ]; then
    echo "====> Unknown cross-compiler - $CROSS_COMPILE"
    echo "====> $SDK_CROSS_UCLIBC_NAME cross-compiler should be used"
    sdk_exit
fi

###echo "====> verify $WRKD/$SDK_APPL_DIR/$SDK_APPL/$SDK_APPL_CLISH_DIR/Makefile"
if [ ! -e "$WRKD/$SDK_APPL_DIR/$SDK_APPL/$SDK_APPL_CLISH_DIR/Makefile" ]; then
    echo "====> Configure CLISH application"
    echo "====> cd $WRKD/$SDK_APPL_DIR/$SDK_APPL"
    cd $WRKD/$SDK_APPL_DIR/$SDK_APPL
    echo "====> make clishlib CROSS_COMPILE=${CROSS_COMPILE}>/dev/null 2>&1"
    make clishlib CROSS_COMPILE=${CROSS_COMPILE}>/dev/null 2>&1
    if [ $? -ne 0 ]
    then
        echo "Failed CLISH configuration"
        sdk_exit
    fi
fi

###echo "====> cd $WRKD/$SDK_APPL_DIR/$SDK_APPL/$SDK_APPL_SQLITE_DIR"
cd $WRKD/$SDK_APPL_DIR/$SDK_APPL/$SDK_APPL_SQLITE_DIR
if [ ! -e "Makefile" ]; then
    echo "====> Configure SQLITE application"
    echo "====> ./configure --host=arm-linux CC=${CROSS_COMPILE}gcc > /dev/null"
    ./configure --host=arm-linux CC=${CROSS_COMPILE}gcc > /dev/null
    if [ $? -ne 0 ]
    then
        echo "Failed SQLITE configuration"
        sdk_exit
    fi
fi

cd $WRKD/../$SDK_TOOLS_DIR
chmod 777 mk*
chmod 777 sdk*

echo "=================================================="
echo "====> Current product is $SDK_PROD"
echo "=================================================="

sdk_print_log
