static int swAddVTU(int argc, char* argv[])
{
	int devIdx, pvid, i, dbNum, sid;
	GT_BOOL stuFound;
	GT_U32 portVec, tagVec, unTagVec;
	GT_VTU_ENTRY entry;
	GT_STU_ENTRY stuEntry;
	GT_STATUS status;

	if((argc < 7) || (argc > 9))
	{
		showUsage();
		return 1;
	}
	
	devIdx  = (int)strtoul(argv[2], NULL, 16);
	dbNum   = (int)strtoul(argv[3], NULL, 16);
	pvid    = (int)strtoul(argv[4], NULL, 16);
	portVec = (GT_U32)strtoul(argv[5], NULL, 16);
	sid     = (int)strtoul(argv[6], NULL, 16);
	tagVec = unTagVec = 0;
	
	if((pvid <= 0) || (pvid > 0xFFF))
	{
		printf("Option %s is used with invalid parameter\n", argv[1]+1);
		printf("parameter \"pvid\" should be between 1 and 4096\n");
		return 1;
	}

	if(argc > 7)
	{
		i = argc - 7;
		for(i=7; i<argc; i++)
		{
			if(*argv[i] == 'T')
			{
				tagVec = (GT_U32)strtoul(argv[i]+1, NULL, 16);
			}
			else if(*argv[i] == 'U')
			{
				unTagVec = (GT_U32)strtoul(argv[i]+1, NULL, 16);
			}
			else
			{
				showUsage();
				return 1;
			}
		}
	}

	if (unTagVec & tagVec)
	{
		printf("A port cannot be in both egress Tagged and egress Untagged\n");
		return 1;
	}

	if ((unTagVec | tagVec) && !(portVec & (unTagVec | tagVec)))
	{
		printf("Port for egress Tagged or Untagged should be in portVec\n");
		return 1;
	}
	
	pQdDev = qddev;

	stuEntry.sid = sid;

	status=gstuFindSidEntry(pQdDev, &stuEntry, &stuFound);
	switch(status)
	{
		case GT_OK:
			break;
		case GT_NO_SUCH:
			for(i=0; i<pQdDev->numOfPorts; i++)
			{
				stuEntry.portState[i] = 0;
			}
			stuFound = GT_TRUE;
			break;

		case GT_NOT_SUPPORTED:
			stuFound = GT_FALSE;
			break;
		default:
			printf("Failed getting STU Entry for sid %i (%i)\n",sid,status);
			stuFound = GT_FALSE;
			break;
	}

	entry.DBNum = (GT_U16)dbNum;
	entry.sid = (GT_U8)sid;
	entry.vid = pvid;
	entry.vidPriOverride = 0;

	for(i=0; i<pQdDev->numOfPorts; i++)
	{
		if((portVec>>i) & 0x1)
		{
			if ((tagVec>>i) & 0x1)
				entry.vtuData.memberTagP[i] = MEMBER_EGRESS_TAGGED;
			else if ((unTagVec>>i) & 0x1)
				entry.vtuData.memberTagP[i] = MEMBER_EGRESS_UNTAGGED;
			else
				entry.vtuData.memberTagP[i] = MEMBER_EGRESS_UNMODIFIED;

			if (stuFound)
			{
				stuEntry.portState[i] = GT_PORT_FORWARDING;
				entry.vtuData.portStateP[i]	= 0;
			}
			else
				entry.vtuData.portStateP[i]	= GT_PORT_FORWARDING;
		}
		else
		{
			entry.vtuData.memberTagP[i] = NOT_A_MEMBER;
			entry.vtuData.portStateP[i]	= 0;
		}
	}
    entry.vidPolicy = GT_FALSE;

	if((status=gvtuAddEntry(pQdDev, &entry)) != GT_OK)
	{
		printf("Adding VTU Entry for vid %i didn't success (%i)\n",pvid,status);
	}
	else
	{
		printf("Adding VTU Entry for vid %i successed\n", pvid);
	}

	if (stuFound != GT_TRUE)
		return status;

	if((status=gstuAddEntry(pQdDev, &stuEntry)) != GT_OK)
	{
		printf("Adding STU Entry for sid %i didn't success (%i)\n",sid,status);
	}
	else
	{
		printf("Adding STU Entry for sid %i successed\n", sid);
	}

	return status;
}
